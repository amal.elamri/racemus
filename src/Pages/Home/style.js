export const styles = () => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    textField: {
        width: 200,
        marginBottom: '20px'
    },
    label: {
        "&$focusedLabel": {
            color: "black"
        }
    },
    underline: {
        "&:after": {
            borderBottom: `2px solid black`
        }
    },
    focusedLabel: {},
    imageSize: {
        maxWidth: "100%",
        maxHeight: "100%"
    },
    headerBg: {
        backgroundColor: "#D52B1E"
    },
    progress: {
        color: "#D52B1E",
        marginTop: '40px',
        marginLeft: '10px'
      }
});
  