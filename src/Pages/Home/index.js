import React from "react";
import Header from "../../Components/Header";
import MapWithAMarker from "../../Components/MapWithAMarker"
import Slides from "../../Components/Slides"
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { ROOT_URL } from "../../config.js";
import { styles } from "./style";
import CircularProgress from '@material-ui/core/CircularProgress';

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            code: '',
            Macdos: [],
            center: {
                lat: 47.751076,
                lng: -120.740135
            },
            isLoading: false
        }
        this.getMacdos = this.getMacdos.bind(this);
    }

    handleChange = () => event => {
        this.setState({ code: event.target.value });
        this.getMacdos(event.target.value);
    };
    
    /*fetch for macdos fastfoods*/
    getMacdos = (code) => {
        this.setState({ isLoading: true });

        fetch(`${ROOT_URL}/api/fastfoods`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                code: code
            })
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState({ isLoading: false });
                if (responseJson.status === "Fail") {
                    this.setState({ Macdos: [] });
                }
                else {
                    this.setState(prevState => {
                        let Macdos = Object.assign({}, prevState.Macdos);
                        Macdos = responseJson.content;
                        return { Macdos };
                    })

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
                console.error(error);
            });
    };

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Header />
                <form className={classes.container} noValidate autoComplete="off">
                    <TextField
                        id="standard-search"
                        label="Search For American State"
                        InputLabelProps={{
                            classes: {
                                root: classes.label,
                                focused: classes.focusedLabel
                            }
                        }}
                        InputProps={{
                            classes: {
                                root: classes.underline
                            }
                        }}
                        type="search"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.code}
                        onChange={this.handleChange(this.state.code)}
                    />
                    {this.state.isLoading && <CircularProgress size={25} className={classes.progress} />}

                </form>

                <MapWithAMarker
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-p5fYllVvDxmm4MNUGaEl0nthJkQ8QMQ"
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `600px` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                    markerpoint={this.state.Macdos}
                    center={this.state.center}
                    isMarkerShown
                />

                <Slides
                    slides={this.state.Macdos}
                    code={this.state.code}
                />
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);
