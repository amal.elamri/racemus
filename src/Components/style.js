export const styles = () => ({
    img: {
        width: '100%',
        float: 'left',
        paddingBottom: '15px'
    },
    card: {
        minHeight: '150px',
        maxWidth: "97%",
        backgroundColor: '#fafafa'
    },
    code: {
        textTransform: 'capitalize',
        fontWeight: 'bold'
    },
    CardContent: {
        display: 'flex',
        flexDirection: 'row',
        paddingBottom: 'unset'
    },
    title: {
        fontSize: '13px',
        fontWeight: 500,
        textAlign: 'left'
    },
    titles: {
        fontSize: '14px',
        fontWeight: 500,
        textAlign: 'left'
    },
    address: {
        width: '75%',
        paddingLeft: '20px'
    },
    state: {
        width: '25%'
    },
    slider: {
        width: '85%',
        marginRight: 'auto',
        marginLeft: 'auto',
        paddingTop: '35px',
        paddingBottom: '35px'
    },
    imageSize: {
        maxWidth: "100%",
        maxHeight: "100%"
    },
    headerBg: {
        backgroundColor: "#D52B1E"
    }
});
