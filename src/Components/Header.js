import React from "react";
import view from "../assets/macmaxi.jpg";
import { styles } from "./style";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
class Header extends React.Component {

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.headerBg} >
                <img src={view} alt="view" className={classes.imageSize} />
            </div>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Header);