import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Slider from "react-slick";
import arrow from "../assets/chips.png";
import macdo from "../assets/macdo.jpg";
import pin from "../assets/pinbox.png";
import { styles } from "./style";

class Slides extends React.Component {
  
    render() {
        const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
          <img
            {...props}
            className={
              "slick-prev slick-arrow" +
              (currentSlide === 0 ? " slick-disabled" : "")
            }
            aria-hidden="true"
            aria-disabled={currentSlide === 0 ? true : false}
            type="button"
            alt="previous"
            src={arrow}
          />
        );
        const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
          <img
            {...props}
            className={
              "slick-next slick-arrow" +
              (currentSlide === slideCount - 1 ? " slick-disabled" : "")
            }
            aria-hidden="true"
            aria-disabled={currentSlide === slideCount - 1 ? true : false}
            type="button"
            alt="next"
            src={arrow}
          />
        );
    
        var settings = {
          dots: false,
          infinite: false,
          speed: 1000,
          slidesToShow:3,
          slidesToScroll: 3,
          autoplay: false,  
          autoplaySpeed: 1000,
          nextArrow: <SlickArrowRight />,
          prevArrow: <SlickArrowLeft /> ,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        };
        const { classes } = this.props;
      
        return (
          <Slider {...settings} className={classes.slider}>
          {this.props.slides.map((marker, i) => {
            return (
              <Card  className={classes.card} key={i}>                         
                  <CardContent className={classes.CardContent}>
                    <div className={classes.state}>
                      <img src={macdo} alt="macdo" className={classes.img}/>                  
                      <p className={classes.code}>{this.props.code}</p>                       
                    </div>
                    <div className={classes.address}>
                      <p className={classes.titles}>{marker.title} </p>                      
                      <p className={classes.title}><img src={pin} alt="pin" style={{float: 'left'}} />{marker.address} </p>     
                    </div>           
                   
                    
                  </CardContent>
              
                
              </Card>
            );
          })}
          </Slider>
        );
    }



}
Slides.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Slides);
