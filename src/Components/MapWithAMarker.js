import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";
import pinbox from "../assets/pin.png";

class MapWithAMarker extends React.Component {

  render() {
    return (
      <GoogleMap
        defaultZoom={9}
        center={this.props.center}
      >
        {this.props.markerpoint.slice(1, 31).map((marker, i) => {
          return (
            <Marker
              key={i}
              position={{
                lat: Number(marker.latitude),
                lng: Number(marker.longitude)
              }}
            >
              <InfoWindow>
                <div style={{fontWeight: 400}}>
                  <img src={pinbox} style={{ width: 25 }} alt="pin" />
                  {marker.title}
                </div>
              </InfoWindow>
            </Marker>
          );
        })}
      </GoogleMap>
    );
  }

}

export default withScriptjs(withGoogleMap(MapWithAMarker));
