import React from 'react';
import App from '../App';

import renderer from 'react-test-renderer'

describe('First snapshot test', () => {
  test('testing app', () => {

    const tree = renderer
      .create(<App />)
      .toJSON();
    expect(tree).toMatchSnapshot();

  })
})